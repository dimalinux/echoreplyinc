/*
 *  IPv6 capable echo client tested on Linux and macOS.  Reads a single line
 *  from standard input, sends it to the echo sever, reads the data back,
 *  prints it to standard out, then exits.
 *
 *  Examples (localhost):
 *
 *       echoclient ::1 8888
 *       echoclient 127.0.0.1 7777
 *
 *  Exiting:
 *
 *       Use EOF (control-D), SIGINT (control-c) or just type "exit" by itself on
 *       an input line.
 */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>

#define ECHO_BUF_SIZE 1024

int connect_to_host(const char *hostname, const char *port);

void echo_loop(FILE *sock_f);

void fatal_error(const char *format, ...);


int main(int argc, char *argv[]) {
    if (argc < 3)
        fatal_error("Usage: %s <server-address> <server-port>\n", argv[0]);

    char *echo_server = argv[1];
    char *echo_port = argv[2];

    int es_fd = connect_to_host(echo_server, echo_port);

    FILE *es_file = fdopen(es_fd, "r+");
    if (es_file == NULL) {
        fatal_error("Failed to convert descriptor to FILE*: %s\n", strerror(errno));
    }

    echo_loop(es_file);

    fclose(es_file);

    return 0;
}


int connect_to_host(const char *hostname, const char *port) {

    struct addrinfo hints = {
            .ai_family = AF_UNSPEC,
            .ai_socktype = SOCK_STREAM,
            .ai_protocol = IPPROTO_TCP,
            .ai_flags = AI_V4MAPPED | AI_ADDRCONFIG
    };
    struct addrinfo *info = NULL;

    int result = getaddrinfo(hostname, port, &hints, &info);
    if (result)
        fatal_error("getaddrinfo: %s\n", gai_strerror(result));

    /*
     *  The "struct addrinfo" retrieved by getaddrinfo is a linked list.
     *  Below we just use the first result and throw an error if it doesn't work.
     *  Since we don't specify "ai_family" in our request, we can expect 2
     *  results frequently: (1) IPv6 address and (2) IPv4 address.  Some
     *  programs chose to loop through the results and connect to the first
     *  successful address.
     */
    int fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
    if (fd < 0)
        fatal_error("Could not create socket\n");

    /*
     * remote_ip is for printing the address.  INET6_ADDRSTRLEN is the longest
     * IPv6 address length as a string including a byte for the '\0' string
     * terminator.
     */
    char remote_ip[INET6_ADDRSTRLEN];
    result = getnameinfo(info->ai_addr, info->ai_addrlen, remote_ip,
                         INET6_ADDRSTRLEN, NULL, 0, NI_NUMERICHOST);
    if (result) fatal_error("getnameinfo: %s\n", gai_strerror(result));

    int err = connect(fd, info->ai_addr, info->ai_addrlen);
    if (err < 0) {
        fatal_error("Could not connect to '[%s]:%s': %s\n",
                    remote_ip, port, strerror(errno));
    }

    printf("Connected to host '%s (%s)' on port %s\n", hostname, remote_ip, port);

    freeaddrinfo(info);

    return fd;
}

void echo_loop(FILE *sock_f) {
    while (1) {
        char buf[ECHO_BUF_SIZE] = {};

        /*
         * Read a line of user input
         */
        printf("Enter text: ");
        if (fgets(buf, sizeof buf, stdin) == NULL) {
            fputc('\n', stdout);
            if (ferror(stdin)) {
                fatal_error("Input error: %s\n", strerror(errno));
            }
            break;  /* EOF */
        }
        if (strcasecmp(buf, "exit\n") == 0) {
            fputc('\n', stdout);
            break;
        }
        /*
         * In the normal case, our input string ends with "\n\0", but if the
         * user gives absurdly long input, we could just get "\0" at the end.
         */
        if (strlen(buf) >= ECHO_BUF_SIZE - 2 && buf[ECHO_BUF_SIZE - 2] != '\n')
            fatal_error("Input too long\n");

        /*
         *  Send user input to echo server
         */
        if (fputs(buf, sock_f) <= 0)
            fatal_error("Failed to write to echo server: %s\n", strerror(errno));

        /*
         * Read back echo
         */
        memset(buf, 0, sizeof(buf)); // proving we have echoed data
        if (fgets(buf, sizeof buf, sock_f) == NULL) {
            if (ferror(stdin))
                fatal_error("Failed to read echo reply: %s\n", strerror(errno));
            printf("Connection closed\n");
            break;
        }

        /*
         * Display echo to user
         */
        printf("Echo: %s", buf);
    }
}

void fatal_error(const char *format, ...) {
    va_list args;
    va_start (args, format);
    vfprintf(stderr, format, args);
    va_end (args);
    fflush(stderr);
    exit(1);
}