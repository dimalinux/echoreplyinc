/*
 *  IPv6 echo server.  Reads and echos input from a single client until
 *  disconnect.  Then waits for next client.
 *
 *  While it is IPv6, if you listen on the wildcard (::) addresses, you can
 *  still receive connections from IPv4-only clients.
 *
 *  Example localhost:
 *
 *       echoserver ::1 8888
 *
 *  Example any-interface:
 *
 *       echoserver :: 8888
 *
 */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>

/*
 * Number of threads (including the main thread) accepting client connections
 */
#define NUM_WORKERS 5

/*
 * The backlog parameter to listen() is advisory, so we'll most likely
 * get a higher value.  I'd actually be fine rejecting clients after
 * NUM_WORKERS is exceeded for a diagnostic tool like this one.
 */
#define LISTEN_BACKLOG 1

#define ECHO_BUF_SIZE 1024

/*
 * strlen("65536") + 1
 */
#define MAX_PORTSTR_LEN 6

/*
 * Our combo string is "[ip]:port".
 * INET6_ADDRSTRLEN already includes terminator
 */
#define MAX_IPPORTSTRLEN (INET6_ADDRSTRLEN + 5 + 3)

int bind_to_socket(const char *listen_addr, const char *port);

void *accept_loop(void *listen_fd_ptr);

void handle_client(int cli_fd, struct sockaddr *cli_sock, socklen_t cli_sock_len);

void fatal_error(const char *format, ...);


int main(int argc, char *argv[]) {

    if (argc < 3)
        fatal_error("Usage: %s <bind-address> <bind-port>\n", argv[0]);

    const char *listen_addr = argv[1];
    const char *listen_port = argv[2];

    int listen_fd = bind_to_socket(listen_addr, listen_port);
    if (listen(listen_fd, LISTEN_BACKLOG) < 0)
        fatal_error("Could not listen on socket: %s\n", strerror(errno));

    // Ignore SIGPIPE when we write to a client socket that is already closed.
    // Instead, write with fail with error EPIPE.
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
        fatal_error("signal\n");

    const unsigned int num_child_thr = NUM_WORKERS - 1;

    for (int i = 0; i < num_child_thr - 1; i++) {
        pthread_t thr;
        int ret = pthread_create(&thr, NULL, accept_loop, &listen_fd);
        if (ret != 0)
            fatal_error("pthread_create error: %s\n", strerror(ret));

        /* not required, but detach since we don't call pthread_join() */
        ret = pthread_detach(thr);
        if (ret != 0)
            fatal_error("pthread_detach error: %s\n", strerror(ret));
    }
    accept_loop(&listen_fd); /* main thread is the final worker */

    /* accept_loop is infinite, the rest is unreachable */

    close(listen_fd);
    return 0;
}


int bind_to_socket(const char *listen_addr, const char *port) {

    struct addrinfo hints = {
            .ai_family = AF_UNSPEC,
            .ai_socktype = SOCK_STREAM,
            .ai_protocol = IPPROTO_TCP,
            .ai_flags = AI_PASSIVE |  /* Used for bind() */
                        AI_V4MAPPED | /* IPv4 mapped to IPv6 is ok */
                        AI_ADDRCONFIG /* Return IPv4 or IPv6 only if host has it */
    };
    struct addrinfo *info = NULL;

    int result = getaddrinfo(listen_addr, port, &hints, &info);
    if (result != 0)
        fatal_error("getaddrinfo: %s\n", gai_strerror(result));

    /*
     *  The user may have passed in "localhost" or some other non-numeric
     *  value. We use getnameinfo(...) to get the exact address and port
     *  used.
     */
    char server_ip[INET6_ADDRSTRLEN] = {};
    char server_port[MAX_PORTSTR_LEN] = {};
    result = getnameinfo(info->ai_addr, info->ai_addrlen,
                         server_ip, sizeof(server_ip),
                         server_port, sizeof(server_port),
                         NI_NUMERICHOST | NI_NUMERICSERV);
    if (result) fatal_error("getnameinfo: %s\n", gai_strerror(result));

    char server_info[MAX_IPPORTSTRLEN] = {};
    sprintf(server_info, "[%s]:%s", server_ip, server_port);

    /*
     *  info is a linked list.  Do we need to ever check past the first result?
     */
    int listen_fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
    if (listen_fd < 0) fatal_error("Could not create socket for %s\n", server_info);

    int reuse = 1;
    if (setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
        fatal_error("enabling SO_REUSEADDR on %s failed: %s\n", server_info, strerror(errno));

    if (info->ai_family == AF_INET6) {
        int v6only = 0;
        if (setsockopt(listen_fd, IPPROTO_IPV6, IPV6_V6ONLY, &v6only, sizeof(v6only)) != 0)
            fatal_error("disabling IPV6_V6ONLY on %s failed: %s\n", server_info, strerror(errno));
    }

    int err = bind(listen_fd, info->ai_addr, info->ai_addrlen);
    if (err < 0)
        fatal_error("Could not bind to %s\n", server_info);

    printf("Server is listening on %s\n", server_info);

    freeaddrinfo(info);
    return listen_fd;
}

/*
 *  Infinite loop accepting connections and servicing clients.  accept() is thread
 *  safe and clients don't interact, so we don't access any complexity for
 *  synchronization.
 */
void *accept_loop(void *listen_fd_ptr) {

    int listen_fd = *(int *) listen_fd_ptr;

    for (;;) {
        struct sockaddr_storage client_sock = {};
        socklen_t client_sock_len = sizeof(client_sock);

        int client_fd = accept(listen_fd, (struct sockaddr *) &client_sock, &client_sock_len);
        if (client_fd < 0) {
            fatal_error("Could not establish new connection\n");
            break;  // avoid any infinite loop warnings
        }

        handle_client(client_fd, (struct sockaddr *) &client_sock, client_sock_len);

        close(client_fd);
    }
    return NULL;
}


void handle_client(int cli_fd, struct sockaddr *cli_sock, socklen_t cli_sock_len) {

    /*
     * NI_MAXHOST and NI_MAXSERV are overkill since we pass
     * (NI_NUMERICHOST | NI_NUMERICSERV) to getnameinfo(...).
     */
    char cli_ip[INET6_ADDRSTRLEN] = {};
    char cli_port[MAX_PORTSTR_LEN] = {};

    int result = getnameinfo(cli_sock, cli_sock_len,
                             cli_ip, sizeof(cli_ip),
                             cli_port, sizeof(cli_port),
                             NI_NUMERICHOST | NI_NUMERICSERV);
    if (result)
        fatal_error("getnameinfo: %s\n", gai_strerror(result));

    char cli_info[NI_MAXHOST];
    sprintf(cli_info, "[%s]:%s", cli_ip, cli_port);

    printf("client %s connected\n", cli_info);

    char echo_buf[ECHO_BUF_SIZE] = {};

    for (;;) {
        ssize_t read = recv(cli_fd, echo_buf, ECHO_BUF_SIZE, 0);

        if (read == 0)
            break; // done reading
        if (read < 0) {
            printf("Client read failed: %s\n", strerror(errno));
            break;
        }

        ssize_t sent = send(cli_fd, echo_buf, read, 0);
        if (sent < 0) {
            printf("Client %s write failed: %s\n", cli_info, strerror(errno));
            break;
        }

        printf("echoed %ld bytes back to %s\n", read, cli_info);
    }
    printf("connection from %s complete\n", cli_info);
}


void fatal_error(const char *format, ...) {
    va_list args;
    va_start (args, format);
    vfprintf(stderr, format, args);
    va_end (args);
    fflush(stderr);
    exit(1);
}
